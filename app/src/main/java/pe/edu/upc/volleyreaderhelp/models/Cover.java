package pe.edu.upc.volleyreaderhelp.models;

/**
 * Created by angelvelasquez on 7/3/15.
 */
public class Cover {
    String smallUrl;
    String mediumUrl;
    String largeUrl;

    public Cover(String smallUrl, String mediumUrl, String largeUrl) {
        this.smallUrl = smallUrl;
        this.mediumUrl = mediumUrl;
        this.largeUrl = largeUrl;
    }
}
