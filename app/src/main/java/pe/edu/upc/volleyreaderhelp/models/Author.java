package pe.edu.upc.volleyreaderhelp.models;

/**
 * Created by angelvelasquez on 7/3/15.
 */
public class Author {
    String key;
    String name;

    public Author(String key, String name) {
        this.key = key;
        this.name = name;
    }

    public String getName() {
        return this.name;
    }
}
