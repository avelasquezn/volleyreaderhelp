package pe.edu.upc.volleyreaderhelp.models;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.util.ArrayList;

import pe.edu.upc.volleyreaderhelp.R;
import pe.edu.upc.volleyreaderhelp.activities.BookItemActivity;

/**
 * Created by angelvelasquez on 7/3/15.
 */
public class BooksAdapter extends RecyclerView.Adapter<BooksAdapter.ViewHolder> {
    ArrayList<Book> books;

    public BooksAdapter(ArrayList<Book> books) {
        this.books = books;
    }

    @Override
    public BooksAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.book_card, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(BooksAdapter.ViewHolder holder, final int position) {
        holder.titleTextView.setText(books.get(position).title);
        holder.authorsTextView.setText(books.get(position).getAuthorsString());
        final Long coverId = books.get(position).coverId;
        String coverUrl = "";
        if(coverId != 0) {
            coverUrl = books.get(position).cover.mediumUrl;
            Picasso.with(holder.itemView.getContext()).load(coverUrl).into(holder.coverImageView);
        }

        final String finalCoverUrl = coverUrl;
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.printf("Selected position: %d%n", position);
                Intent bookItemIntent = new Intent(view.getContext(), BookItemActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("key", books.get(position).key);
                bundle.putString("title", books.get(position).title);
                bundle.putString("authors", books.get(position).getAuthorsString());
                bundle.putLong("coverId", coverId);
                bundle.putString("coverUrl", finalCoverUrl);
                bookItemIntent.putExtras(bundle);
                view.getContext().startActivity(bookItemIntent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return books.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView titleTextView;
        ImageView coverImageView;
        TextView authorsTextView;
        public ViewHolder(View itemView) {
            super(itemView);
            titleTextView = (TextView) itemView.findViewById(R.id.titleTextView);
            coverImageView = (ImageView) itemView.findViewById(R.id.coverImageView);
            authorsTextView = (TextView) itemView.findViewById(R.id.authorsTextView);
        }
    }
}
