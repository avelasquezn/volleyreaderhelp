package pe.edu.upc.volleyreaderhelp.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;

import pe.edu.upc.volleyreaderhelp.R;
import pe.edu.upc.volleyreaderhelp.models.Book;
import pe.edu.upc.volleyreaderhelp.models.BooksAdapter;

public class BookCatalogActivity extends AppCompatActivity {
    ArrayList<Book> books;
    private RecyclerView mBooksRecyclerView;
    private RecyclerView.Adapter mBooksAdapter;
    private RecyclerView.LayoutManager mBooksLayoutManager;
    Button newSearchButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_catalog);
        mBooksRecyclerView = (RecyclerView) findViewById(R.id.booksRecyclerView);
        mBooksRecyclerView.setHasFixedSize(true);
        mBooksLayoutManager = new LinearLayoutManager(this);
        mBooksRecyclerView.setLayoutManager(mBooksLayoutManager);
        books = new ArrayList<>();
        books.addAll(MainActivity.getBooks());
        System.out.println("BookCatalogActivity: Array Size "+ Integer.toString(books.size()));
        mBooksAdapter = new BooksAdapter(books);
        mBooksRecyclerView.setAdapter(mBooksAdapter);
        newSearchButton = (Button) findViewById(R.id.newSearchButton);
        newSearchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_book_catalog, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    public void clearData() {
        books.removeAll(books);
    }

    public void initializeData() {
        books = new ArrayList<>();


    }

}
