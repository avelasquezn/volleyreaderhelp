package pe.edu.upc.volleyreaderhelp.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

import pe.edu.upc.volleyreaderhelp.R;
import pe.edu.upc.volleyreaderhelp.models.Author;
import pe.edu.upc.volleyreaderhelp.models.Book;
import pe.edu.upc.volleyreaderhelp.models.Cover;

public class MainActivity extends AppCompatActivity {
    private static ArrayList<Book> books = new ArrayList<>();
    private static String BOOKS_SEARCH_URL = "http://openlibrary.org/search.json?title=";
    private static String COVERS_SEARCH_URL = "http://covers.openlibrary.org/b/id/";
    EditText searchEditText;
    Button searchButton;

    public static ArrayList<Book> getBooks() {
        return books;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        searchEditText = (EditText) findViewById(R.id.searchEditText);
        searchButton = (Button) findViewById(R.id.searchButton);
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String searchString = null;
                try {
                    searchString = URLEncoder.encode(searchEditText.getText().toString(), "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

                if (searchString.length() > 0) {
                    searchTitles(BOOKS_SEARCH_URL + searchString);
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void searchTitles(String searchTitleUrl) {
        System.out.println("URL = " + searchTitleUrl);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(
                Request.Method.GET, searchTitleUrl, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    // the response is already constructed as a JSONObject!
                    try {
                        books.clear();
                        int resultsCount = response.getInt("numFound");
                        JSONArray resultsArray = response.getJSONArray("docs");

                        System.out.println("Results: " + resultsCount);

                        int limit = resultsCount > 10 ? 10 : resultsCount;

                        for (int position = 0; position<limit ; position++){
                            JSONObject result = resultsArray.getJSONObject(position);
                            String title = result.getString("title");
                            System.out.println("Title = " + title);
                            String key = result.getString("key");
                            Long coverId = result.optLong("cover_i");
                            System.out.println("cover_i = " + coverId);
                            Boolean hasFullText = result.getBoolean("has_fulltext");
                            int firstPublishYear = result.optInt("first_publish_year");
                            int editionCount = result.getInt("edition_count");

                            JSONArray authorNames = result.optJSONArray("author_name");
                            JSONArray authorKeys = result.optJSONArray("author_key");
                            int authorsCount = authorNames == null ? 0 : authorNames.length();
                            ArrayList<Author> authors = new ArrayList<>();
                            for (int authorPosition = 0; authorPosition < authorsCount; authorPosition++) {
                                Author author = new Author(authorKeys.getString(authorPosition), authorNames.getString(authorPosition));
                                authors.add(author);
                                System.out.println("Author name = " + author.getName());
                            }

                            Cover cover = new Cover(
                                    COVERS_SEARCH_URL + coverId + "-S.jpg",
                                    COVERS_SEARCH_URL + coverId + "-M.jpg",
                                    COVERS_SEARCH_URL + coverId + "-L.jpg"
                            );

                            Book book = new Book(key, coverId, hasFullText, title, authors, cover, firstPublishYear, editionCount);
                            books.add(book);
                            System.out.println("Book title = " + book.getTitle());
                        }
                        System.out.println("searchTitles: Array Size "+ Integer.toString(books.size()));
                        if(books.size() > 0) {
                            Intent intent = new Intent(MainActivity.this, BookCatalogActivity.class);
                            startActivity(intent);
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                }
            }
        );
        Volley.newRequestQueue(this).add(jsonRequest);

    }
}
