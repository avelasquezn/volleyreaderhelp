package pe.edu.upc.volleyreaderhelp.models;


import java.util.ArrayList;

/**
 * Created by angelvelasquez on 7/3/15.
 */
public class Book {
    String key;
    Long coverId;
    Boolean hasFullText;
    String title;
    ArrayList<Author> authors;
    Cover cover;
    int firstPublishYear;
    int editionCount;

    public Book(String key, Long coverId, Boolean hasFullText, String title, ArrayList<Author> authors, Cover cover, int firstPublishYear, int editionCount) {
        this.key = key;
        this.coverId = coverId;
        this.hasFullText = hasFullText;
        this.title = title;
        this.authors = authors;
        this.cover = cover;
        this.firstPublishYear = firstPublishYear;
        this.editionCount = editionCount;
    }

    public String getAuthorsString() {
        String authorsString = "";
        Integer counter = 0;
        for (Author author:authors) {
            authorsString += author.name;
            if(++counter < authors.size()) {
                authorsString += ", ";
            }
        }
        return authorsString;
    }

    public String getTitle() {
        return title;
    }

}
