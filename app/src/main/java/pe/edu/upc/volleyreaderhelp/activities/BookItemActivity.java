package pe.edu.upc.volleyreaderhelp.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import pe.edu.upc.volleyreaderhelp.R;

public class BookItemActivity extends AppCompatActivity {
    TextView titleTextView;
    TextView authorsTextView;
    TextView keyTextView;
    ImageView coverImageView;
    Button backButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_item);

        Bundle bundle = getIntent().getExtras();
        titleTextView = (TextView) findViewById(R.id.titleTextView);
        titleTextView.setText(bundle.getString("title"));
        authorsTextView = (TextView) findViewById(R.id.authorsTextView);
        authorsTextView.setText(bundle.getString("authors"));
        keyTextView = (TextView) findViewById(R.id.keyTextView);
        keyTextView.setText(bundle.getString("key"));
        coverImageView = (ImageView) findViewById(R.id.coverImageView);
        Long coverId = bundle.getLong("coverId");
        if(coverId != 0) {
            String coverUrl = bundle.getString("coverUrl");
            Picasso.with(this.getBaseContext()).load(coverUrl).into(coverImageView);
        }


        backButton = (Button) findViewById(R.id.backButton);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_book_item, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
